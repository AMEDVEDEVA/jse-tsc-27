package ru.tsc.golovina.tm.dto;

import lombok.Getter;
import lombok.Setter;
import ru.tsc.golovina.tm.model.Project;
import ru.tsc.golovina.tm.model.Task;
import ru.tsc.golovina.tm.model.User;

import java.io.Serializable;
import java.util.List;


@Getter
@Setter
public class Domain implements Serializable {

    private List<Project> projects;

    private List<Task> tasks;

    private List<User> users;

}
